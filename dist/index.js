// Copyright 2018  Rio Advancement Inc

// See LICENSE file

'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

import R from 'ramda';
import cities from 'cities.json';

var _fromCity = function _fromCity(city) {
  return city.name + ", " + city.country;
};

var _fromString = function _fromString(city, country) {
  return city + ", " + country;
};

// Group function that groups the cities json by the format as below
// 'City name, Country' : [{ its lattitue data }] 
//
var byCityWithCountry = R.groupBy(function (city) {
  return _fromCity(city);
});
// Apply the byCityWithCountry groupBy function with the cities data
// The locations has the transformed data.
// Examples
// 'Delhi, IN': [{"country":"IN","name":"New Delhi","lat":"28.63576","lng":"77.22445"}]
//
var locations = byCityWithCountry(cities);

// Only store the keys of the locations
//
var locationKeys = R.keys(locations);

// A scoped cached, that stored the cached results.
var cachedLocations = {};

var RioGeo = function () {
  function RioGeo() {
    _classCallCheck(this, RioGeo);
  }

  // Given a city and country name, return
  // multiple cities + countries combo found.
  // This is an opinionated function for Rio. 
  // This works in the way that when an  user types
  // "NEW" + US => The  multiple cities starting with NEW + US 
  // must be sent back.


  _createClass(RioGeo, [{
    key: 'locateCity',
    value: function locateCity(city, country) {
      var uCity = city.toUpperCase();
      var uCountry = country.toUpperCase();

      var isLike = function isLike(n) {
        var uN = n.toUpperCase();
        var regex = new RegExp('.*, ' + uCountry, 'g');
        var res = uN.match(regex);
        return res != null && res.length > 0 && uN.indexOf(uCity) >= 0;
      };

      var composeFn = R.compose(R.filter(isLike));

      return composeFn(locationKeys);
    }

    // Exported search function that does the following
    // When provided a city and a country name
    // Tries to get the latitude/longitude from the cache, 
    // If not does a live search from the cities file

  }, {
    key: 'fillWithGeoInfo',
    value: function fillWithGeoInfo(city, country) {
      var force = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

      var found = cachedLocations[_fromString(city, country)];

      if (!found || force) {
        found = this._searchLive(city, country);

        cachedLocations[_fromString(city, country)] = found;
      }

      return found;
    }

    // Does the following computations
    // 1. Filters the locationKey which is "city, country" from the locationsKey
    // 2. If multiple are found then takes the first
    // 3. Expands the locationKey to a location by finding it in the locations
    //    object
    // 4. Flattens as the output from the above step is an [[]]
    // 5. If there are multiple matches, sorts the unique, usually the first one
    // is returned.

  }, {
    key: '_searchLive',
    value: function _searchLive(city, country) {

      var isLike = function isLike(n) {
        return n.indexOf(country) > 0 && n.indexOf(city) >= 0;
      };

      var expLocation = function expLocation(x) {
        return locations[x];
      };

      var countryEq = function countryEq(a, b) {
        return R.eqBy(function (e) {
          return a.country == b.country;
        });
      };

      var composeFn = R.compose(R.uniqWith(countryEq), R.flatten, R.map(expLocation), R.take(1), R.filter(isLike));

      return composeFn(locationKeys);
    }
  }]);

  return RioGeo;
}();

export default RioGeo;